package com.example.shop.repositories;

import com.example.shop.entities.Products;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends JpaRepository<Products,Long> {

}
