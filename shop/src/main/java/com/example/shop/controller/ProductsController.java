package com.example.shop.controller;

import com.example.shop.dto.CreateProductRequest;
import com.example.shop.dto.ProductResponse;
import com.example.shop.entities.Products;
import com.example.shop.services.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/shop/v1/products")
public class ProductsController {
    @Autowired
    private ProductService services;

    @ApiOperation("v1 Read Operation for Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucessfully view product list"),
            @ApiResponse(code= 404, message = "Data not found")

    })
    @GetMapping
    public ResponseEntity<List<ProductResponse>> getAllProducts() {
        return ResponseEntity.ok(services.getProductList());
    }

    @ApiOperation("v1 Read Operation by id for Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucessfully view product by id"),
            @ApiResponse(code= 404, message = "Data not found")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ProductResponse> getProductById(@PathVariable Long id) {
        return ResponseEntity.ok().body(this.services.getProductById(id));
    }

    @ApiOperation("v1 Create Operation for Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucessfully create product")
    })
    @PostMapping
    public ResponseEntity<ProductResponse> addUser(@RequestBody CreateProductRequest product) {
        return ResponseEntity.ok(this.services.createProduct(product));
    }

    @ApiOperation("v1 Update Operation for Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucessfully update product")
    })
    @PutMapping
    public ResponseEntity<ProductResponse> updateProduct(@RequestBody Products product) {
        return ResponseEntity.ok().body(this.services.updateProductById(product));
    }

    @ApiOperation("v1 Delete Operation for Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucessfully delete product")
    })
    @DeleteMapping("/{id}")
    public HttpStatus deleteProduct(@PathVariable Long id) {
        this.services.deleteProductById(id);
        return HttpStatus.OK;
    }
}
