package com.example.shop.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;



import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="products")
@Getter
@Setter
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The database generated product ID")
    private Long id;

    @ApiModelProperty(notes = "The product name",example = "mobil")
    private String name;
    @ApiModelProperty(notes = "The product description",example = "salah satu product yang dijual untuk kebutuhan transportasi")
    @Column(columnDefinition = "TEXT")
    private String description;
    @ApiModelProperty(notes = "The price of product",example = "500000000")
    private double price;
    @ApiModelProperty(notes = "The stock of product",example = "100")
    private int stock;



}
